package com.magorasystems.queue;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class DateStrMessage implements Serializable {
    private Integer number;
    private Date date;
    private String message;
}
