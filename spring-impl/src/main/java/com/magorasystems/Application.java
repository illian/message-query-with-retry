package com.magorasystems;

import com.magorasystems.config.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@EnableAutoConfiguration
@ComponentScan
@Import(Config.class)
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
