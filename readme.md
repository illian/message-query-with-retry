#Description
Example configurations RabbitMQ for retry error messages.
Repository contains two implementation:

 - based on Spring implementation (module **spring-iml**).
 - based on RabbitMq implementation (module **mq-iml**).

Detail description of implementations in modules.

#Описание
Пример конкфигурации RabbitMQ для повторной доставки сообщений в случае ошибки.
Репозиторий содержит две реализации:

 - реализация, основанная на Spring (модуль **spring-iml**).
 - реализация, основанная на RabbitMq (модуль **mq-iml**).

Подробное описание реализаций находится в модулях.