# RabbitMQ retry, MQ implementation (rus)

##Описание

Повторная доставка сообщений через заданные интервалы времени реализована методами брокера сообщений.

Принцип работы заключается в том, что при возникновении ошибки она обрабатывается в коде и сообщение заново отправляется в очередь, но не в ту, из которой поступило сообщение, а в специально настроенную (Dead Letter queue). В этой очереди содержится информация о том, в какую очередь отправить сообщение повторно, и сколько времени должно пройти перед отправкой. Это обозначает, что если вся логика повторной доставки сообщений реализуется брокером сообщений, то ошибочные сообщения не блокируют очередь.

Параметры, определяющие интервал времени ожидания и очередь, в которую будут отправлены сообщения - это параметры очереди, и они задаются при создании этой очереди.

##Реализация

###Конфигурация RabbitMQ (java-config)

Нужно создать еще одну очередь и RabbitTemplate для нее, например так:

```java
@Autowired
private final ConnectionFactory connectionFactory;

@Bean
public Queue outgoingQueue() {
    Map<String, Object> args = new HashMap<>();
    args.put("x-dead-letter-exchange", "");
    args.put("x-dead-letter-routing-key", queueName);
    args.put("x-message-ttl", retryInterval);

    return new Queue(queueNameDeadLetter, false, false, false, args);
}

@Bean (name = DEAD_LETTER_QUEUE)
public RabbitTemplate rabbitTemplateDeadLetter() {
    RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setQueue(outgoingQueue().getName());
    rabbitTemplate.setRoutingKey(outgoingQueue().getName());

    return rabbitTemplate;
}
```

Как видно, для RabbitTemplate указывается идентификатор, так как при автоконфигурации создается один бин RabbitTemplate.

###Код отправителя сообщений

К сообщению необходимо добавить messageProperties, так как, в случае, если возникнет ошибочное сообщение (которое будет отправлено в дополнительную очередь и перенаправлено в основную и получено), при получении оно будет содержать messageProperties, в которых будет указана дополнительная информация по переотправке, и если изначально сообщение было без нее, возникнет ошибка преобразования.

```java
MessageConverter messageConverter;

RabbitTemplate template;
Queue queue;


MessageProperties messageProperties = MessagePropertiesBuilder.newInstance() .build();
Message msg = messageConverter.toMessage(message, messageProperties);
this.template.convertAndSend(queueName, msg);
```

###Код получателя сообщений

В случае, если возникла ошибка, ее нужно перехватить и повторно отправить сообщение, но уже в другую очередь. Ниже видно, что используется Qualifier для получения определенного ,сконфигурированного ранее, бина.

```java
@Qualifier(Config.DEAD_LETTER_QUEUE) RabbitTemplate template;
MessageConverter messageConverter;

template.convertAndSend(message);
```

Если сообщение пришло из дополнительной очереди для ошибок, оно будет содержать присоединенную информацию ,которую можно прочитать.

Например, так можно получить информацию о том, сколько попыток доставки сообщений было выполнено, и если оно больше порогового, то не отправлять сообщение в очередь повторно:

```java
Map<String, Object> headers = message.getMessageProperties().getHeaders();
ArrayList<Map<String, Object>> xDeath = (ArrayList)headers.get("x-death");
if (xDeath != null) {
    Map<String, Object> xDeathMap = xDeath.get(0);
    Long count = (Long)xDeathMap.get("count");
}
```

##Достоинства

Очередь не будет блокирована ошибочными сообщениями.

##Недостатки

Сложнее в конфигурировании.

Чтобы изменить интервал ожидания сообщения в очереди перед отправкой, нужно будет пересоздать очередь, так как этот параметр - часть конфигурации очереди.

Нужно явно перехватывать исключение при работе с очередью и отправлять сообщение повторно.
