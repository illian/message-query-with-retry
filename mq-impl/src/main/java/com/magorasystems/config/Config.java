package com.magorasystems.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Slf4j
@EnableScheduling
@Configuration
public class Config {
    public static final String RABBIT_MQ_QUEUE_NAME_VALUE = "${spring.rabbitmq.queueName:default_queue_name}";
    private static final String RABBIT_MQ_RETRY_INTERVAL = "${spring.rabbitmq.retry:5000}";
    private static final String RABBIT_MQ_DEAD_LETTER_QUEUE_NAME_VALUE =
            "${spring.rabbitmq.queueNameDeadLetter:default_queue_name_dead_letter}";

    public static final String DEAD_LETTER_QUEUE = "deadLetterQueue";

    @Value(RABBIT_MQ_QUEUE_NAME_VALUE)
    private String queueName;

    @Value(RABBIT_MQ_RETRY_INTERVAL)
    private Integer retryInterval;

    @Value(RABBIT_MQ_DEAD_LETTER_QUEUE_NAME_VALUE)
    private String queueNameDeadLetter;


    private final ConnectionFactory connectionFactory;

    @Autowired
    public Config(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Bean
    public Queue queue() {
        return new Queue(queueName);
    }

    @Bean
    public Queue outgoingQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange", "");
        args.put("x-dead-letter-routing-key", queueName);
        args.put("x-message-ttl", retryInterval);

        return new Queue(queueNameDeadLetter, false, false, false, args);
    }

    @Bean (name = DEAD_LETTER_QUEUE)
    public RabbitTemplate rabbitTemplateDeadLetter() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setQueue(outgoingQueue().getName());
        rabbitTemplate.setRoutingKey(outgoingQueue().getName());

        return rabbitTemplate;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
