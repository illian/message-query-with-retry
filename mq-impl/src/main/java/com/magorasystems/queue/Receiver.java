package com.magorasystems.queue;

import com.magorasystems.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Component
@Slf4j
public class Receiver implements MessageListener {
    private final RabbitTemplate template;
    private final MessageConverter messageConverter;

    @Autowired
    public Receiver(@Qualifier(Config.DEAD_LETTER_QUEUE) RabbitTemplate template, MessageConverter messageConverter) {
        this.template = template;
        this.messageConverter = messageConverter;
    }

    @RabbitListener(queues = Config.RABBIT_MQ_QUEUE_NAME_VALUE)
    @Override
    public void onMessage(Message message) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        DateStrMessage dateStrMessage = (DateStrMessage) messageConverter.fromMessage(message);
        log.info("Received: " + dateStrMessage.getMessage() + " / " + simpleDateFormat.format(new Date()));

        if (dateStrMessage.getNumber() == 2) {
            log.error("--Rejected: " + dateStrMessage.getMessage());
            template.convertAndSend(message);
        }
    }
}
