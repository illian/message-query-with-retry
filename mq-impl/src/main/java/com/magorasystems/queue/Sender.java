package com.magorasystems.queue;

import com.magorasystems.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Component
@Slf4j
public class Sender {
    @Value(Config.RABBIT_MQ_QUEUE_NAME_VALUE)
    private String queueName;
    private AtomicInteger counter;

    private RabbitTemplate template;
    private MessageConverter messageConverter;

    @Autowired
    public Sender(RabbitTemplate template, MessageConverter messageConverter) {
        this.template = template;
        this.messageConverter = messageConverter;
        counter = new AtomicInteger(1);
    }

    @Scheduled(fixedDelay = 2000, initialDelay = 500)
    public void send() {
        int number = counter.getAndIncrement();

        MessageProperties messageProperties = MessagePropertiesBuilder.newInstance()
                //.setMessageId(UUID.randomUUID().toString())
                .build();
        String message = "Message #" + number;
        Message msg = messageConverter.toMessage(new DateStrMessage(number, new Date(), message), messageProperties);
        this.template.convertAndSend(queueName, msg);
        log.info("Sent: " + message);
    }
}
